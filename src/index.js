import React from 'react';
import ReactDOM from 'react-dom';

/* REDUX */
import { Provider } from 'react-redux'
import reducers from './reducers';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

/* Router */
import Router from './router';

const store = createStore(reducers, applyMiddleware(thunk));

ReactDOM.render((
  <Provider store={store}>
    <Router/>
  </Provider>
), document.getElementById('root'));
