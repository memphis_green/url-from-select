import React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom';

/* Components */
import App from './containers/App';

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/:service?/:brand?/:style?" component={App}/>
    </Switch>
  </BrowserRouter>
);

export default Router;