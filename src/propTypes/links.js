import { shape, arrayOf, object, oneOf, bool } from 'prop-types';
import { entitiesNames } from '../constants/links';

export const REDUX_LINKS_TYPES = shape({
  services: arrayOf(object).isRequired,
  brands: arrayOf(object).isRequired,
  styles: arrayOf(object).isRequired,
  fetchedEntities: arrayOf(oneOf(entitiesNames)),
  isLinkParsed: bool.isRequired
});