import { SET_SERVICES, SET_BRANDS, SET_STYLES, SET_LINK_PARSED } from '../actions/types';

const INITIAL_STATE = {
  services: [],
  brands: [],
  styles: [],
  fetchedEntities: [],
  isLinkParsed: false
};

export default (state = INITIAL_STATE, action) => {
  const { type: actionType, payload } = action;

  switch (actionType) {
    case SET_SERVICES: {
      const { items } = payload;
      const { fetchedEntities } = state;

      return {
        ...state,
        services: items,
        fetchedEntities: [...fetchedEntities, 'services']
      };
    }
    case SET_BRANDS: {
      const { items } = payload;
      const { fetchedEntities } = state;

      return {
        ...state,
        brands: items,
        fetchedEntities: [...fetchedEntities, 'brands']
      };
    }
    case SET_STYLES: {
      const { items } = payload;
      const { fetchedEntities } = state;

      return {
        ...state,
        styles: items,
        fetchedEntities: [...fetchedEntities, 'styles']
      };
    }
    case SET_LINK_PARSED: {
      const { service, brand, style } = payload;

      return {
        ...state,
        services: service ? [service] : [],
        brands: brand ? [brand] : [],
        styles: style ? [style] : [],
        isLinkParsed: true
      };
    }
    default:
      return state;
  }
};