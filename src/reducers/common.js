import { TOGGLE_SPINNER } from '../actions/types';

const INITIAL_STATE = {
  isSpinner: false
};

export default (state = INITIAL_STATE, action) => {
  const { type } = action;

  switch (type) {
    case TOGGLE_SPINNER: {
      return {
        ...state,
        isSpinner: !state.isSpinner
      };
    }
    default:
      return state;
  }
};