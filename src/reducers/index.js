import { combineReducers } from 'redux';

/* Reducers */
import links from './links';
import common from './common';

const combinedReducers = combineReducers({
  links,
  common
});

export default combinedReducers;