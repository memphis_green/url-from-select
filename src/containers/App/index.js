import React, { useEffect, useState } from 'react';

/* PropTypes */
import { func, bool } from 'prop-types';
import { REDUX_LINKS_TYPES } from '../../propTypes/links';

/* REDUX */
import { connect } from 'react-redux';
import { getServices, getBrands, getStyles, parseLink } from '../../actions/links';

/* Helpers */
import { getUrlSting, getValueFromParam, isEntityFetched, checkAllParams } from '../../helpers/links';
import { entitiesNames } from '../../constants/links';

/* Components */
import Spinner from '../../components/Spinner';
import Select from '../../components/Select';
import { useHistory, useParams } from 'react-router-dom';

const App = (props) => {
  const history = useHistory();
  const {
    service: serviceParam,
    brand: brandParam,
    style: styleParams
  } = useParams();
  const [service, useServices] = useState(getValueFromParam(serviceParam, 's'));
  const [brand, useBrand] = useState(checkAllParams([serviceParam, brandParam], 'b'));
  const [style, useStyle] = useState(checkAllParams([serviceParam, brandParam, styleParams], 'st'));
  const { getServices, links, isLoading, getBrands, getStyles, parseLink } = props;
  const { services, brands, styles, fetchedEntities, isLinkParsed } = links;

  const isFetchedWrapper = (name = '') => isEntityFetched(fetchedEntities, name);
  const [isServicesFetched, isBrandsFetched, isStylesFetched] = entitiesNames.map(name => isFetchedWrapper(name));

  useEffect(() => {
    !isLinkParsed
    && parseLink({
      service,
      brand,
      style
    });
    !isServicesFetched && getServices();
    !isBrandsFetched && getBrands();
    !isStylesFetched && getStyles();

    const url = getUrlSting(service, brand, style);
    url && history.push(url);
  }, [service, brand, style]);


  return isLoading
    ? (
      <Spinner/>
    )
    : (
      <div>
        <p>Services</p>
        <Select
          options={services}
          onSelect={useServices}
          value={service}
          isFetched={isServicesFetched}
        />
        <p>Brands</p>
        <Select
          options={brands}
          onSelect={useBrand}
          value={brand}
          isFetched={isBrandsFetched}
        />
        <p>Styles</p>
        <Select
          options={styles}
          onSelect={useStyle}
          value={style}
          isFetched={isStylesFetched}
        />
      </div>
    );
};

App.propTypes = {
  getServices: func.isRequired,
  getBrands: func.isRequired,
  getStyles: func.isRequired,
  parseLink: func.isRequired,
  links: REDUX_LINKS_TYPES.isRequired,
  isLoading: bool.isRequired
};

const mapStateToProps = ({ links, common }) => ({
  links,
  isLoading: common.isSpinner
});

const mapDispatchToProps = {
  getServices,
  getBrands,
  getStyles,
  parseLink
};

export default connect(mapStateToProps, mapDispatchToProps)(App);