import React from 'react';

/* PropTypes */
import { number } from 'prop-types';

/* Components */
import ClipLoader from 'react-spinners/ClipLoader';

const Spinner = ({ size }) => (
  <div className="sweet-loading">
    <ClipLoader
      size={size}
      color="#123abc"
      loading
    />
  </div>
);

Spinner.propTypes = {
  size: number
};

Spinner.defaultProps = {
  size: 150
};

export default Spinner;