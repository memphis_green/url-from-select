import React from 'react';

import { arrayOf, object, func, string, bool } from 'prop-types';

/* Styles */
import './style.scss';

/* Components */
import Spinner from '../Spinner';

const Select = (props) => {
  const { options, onSelect, value, isFetched } = props;

  return (
    <div className="SelectBlock">
      <select
        onChange={({ target }) => {
          const { value } = target;
          onSelect(value);
        }}
        value={value}
        disabled={!isFetched}
      >
        <option/>
        {options.map(({ id, label, slug }) => (
          <option
            key={id}
            value={slug}
          >
            {label}
          </option>
        ))}
      </select>
      {!isFetched && <Spinner size={30}/>}
    </div>
  );
};

Select.propTypes = {
  options: arrayOf(object).isRequired,
  onSelect: func.isRequired,
  value: string.isRequired,
  isFetched: bool.isRequired
};

export default Select;