/* COMMON */
export const TOGGLE_SPINNER = 'TOGGLE_SPINNER';

/* LINKS */
export const SET_SERVICES = 'SET_SERVICES';
export const SET_BRANDS = 'SET_BRANDS';
export const SET_STYLES = 'SET_STYLES';
export const SET_LINK_PARSED = 'SET_LINK_PARSED';