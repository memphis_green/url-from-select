import axios from 'axios';
import { hostname } from '../constants/API';
import { SET_SERVICES, TOGGLE_SPINNER, SET_BRANDS, SET_STYLES, SET_LINK_PARSED } from './types';
import { isValidData } from '../helpers/links';

export const getServices = () => async dispatch => {
  try {
    const { data } = await axios.get(`${hostname}/search/terms`);
    dispatch({
      type: SET_SERVICES,
      payload: {
        items: data.data
      }
    });
  } catch (e) {
    console.log('Error: ', e);
  }
};

export const getBrands = () => async dispatch => {
  try {
    const { data } = await axios.get(`${hostname}/search/brands_terms`);
    dispatch({
      type: SET_BRANDS,
      payload: {
        items: data.data
      }
    });
  } catch (e) {
    console.log('Error: ', e);
  }
};

export const getStyles = () => async dispatch => {
  try {
    const { data } = await axios.get(`${hostname}/search/styles`);
    dispatch({
      type: SET_STYLES,
      payload: {
        items: data.data
      }
    });
  } catch (e) {
    console.log('Error: ', e);
  }
};

export const parseLink = ({ service: service_slug = '', brand: brand_slug = '', style: style_slug = ''}) => async dispatch => {
  dispatch({
    type: TOGGLE_SPINNER
  });
  try {
    const { data } = await axios.get(`${hostname}/search/parse_link`, {
      params: {
        service_slug,
        brand_slug,
        style_slug
      }
    });
    const { service, brand, style } = data;

    dispatch({
      type: SET_LINK_PARSED,
      payload: {
        service: isValidData(service) ? service : null,
        brand: isValidData(brand) ? brand : null,
        style: isValidData(style) ? style : null
      }
    });
  } catch (e) {
    console.log('Error: ', e);
  }
  dispatch({
    type: TOGGLE_SPINNER
  });
};