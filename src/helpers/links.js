const getUrlItem = (val, prefix) => val ? `/${prefix}-${val}` : '';

export const getUrlSting = (service = '', brand = '', style = '') =>
  `${getUrlItem(service, 's')}${getUrlItem(brand, 'b')}${getUrlItem(style, 'st')}`;

export const getValueFromParam = (param, prefix) => {
  if (param) {
    const [paramPrefix, ...values] = param.split('-');
    return paramPrefix === prefix
      ? values.reduce((acc, val) => `${acc}-${val}`)
      : '';
  }
  return '';
};

export const checkAllParams = (params, prefix) => {
  const param = params.findIndex(
    param => param && param.split('-')[0] === prefix
  );
  return getValueFromParam(params[param], prefix);
};

export const isEntityFetched = (entities = [], name = '') => entities.includes(name);

export const isValidData = item => item.hasOwnProperty('id') && item.hasOwnProperty('label') && item.hasOwnProperty('slug');