module.exports = {
  "env": {
    "browser": true,
    "es6": true
  },
  "extends": "airbnb",
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 2018
  },
  "rules": {
    "no-underscore-dangle:": 0,
    "no-console": 0,
    "comma-dangle": 0,
    "import/order": 0,
    "global-require": 0,
    "no-unused-expressions": 0,
    "consistent-return": 0,
    "no-trailing-spaces": 0,
    "object-curly-newline": 0,
    "no-nested-ternary": 0,
    "no-prototype-builtins": 0,
    "camelcase": 0,
    "arrow-parens": 0,
    "indent": ["error", 2, { SwitchCase: 1 }],
    "no-case-declarations": 0,
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "react/jsx-tag-spacing": 0,
    "react/jsx-first-prop-new-line": 0,
    "react/jsx-max-props-per-line": 0,
    "react/jsx-closing-bracket-location": 0,
    "react/jsx-fragments": 0,
    'import/no-unresolved': 0,
    "max-len": 0,
    "no-useless-constructor": 0,
    "no-shadow": 0,
    "eol-last": 0,
    "import/no-extraneous-dependencies": ["error", { "devDependencies": true }],
    "spaced-comment": 0,
    "no-tabs": 0,
    "react/jsx-one-expression-per-line": 0,
    "no-confusing-arrow": 0,
    "react/no-array-index-key": 0
  }
};
